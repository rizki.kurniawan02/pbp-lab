**1. Apakah perbedaan antara JSON dan XML?**
- XML menggunakan start dan end tag, sedangkan JSON tidak menggunakkan end tag
- JSON didasarkan pada Javascript sementara XML merupakan turunan dari SGML (Standard Generalized Markup Language)
- JSON mendukung array sedangkan XML tidak
- XML mendukung namespace dan comment sedangkan JSON tidak
- Sintaks XML mirip dengan HTML sedangkan JSON mirip dengan Object pada Javascript atau Map pada Python
- JSON lebih mudah dibaca oleh manusia dibandingkan XML
<br>

**2. Apakah perbedaan antara HTML dan XML?**
- Dalam XML user dapat mendefinisakan tipe tag baru, sedangkan pada HTML semua tag sudah didefinisikan sebelumnya
- HTML hanya digunakkan untuk menampilkan data, sedangkan XML hanya digunakkan untuk mengirimkan data dari/ke server
- Tag penutup tidak terlalu penting di HTML
- HTML static sedangkan XML dynamic
- XML case-sensitive sedangkan HTML tidak

Sumber referensi:
1. https://www.geeksforgeeks.org/difference-between-json-and-xml/
2. https://www.geeksforgeeks.org/html-vs-xml/
