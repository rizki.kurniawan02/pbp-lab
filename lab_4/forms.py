from django import forms

from lab_2.models import Note
from django.forms import Widget, widgets
class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = "__all__"
        widgets = {
            "To" : forms.TextInput(attrs={'size': 50}),
            "From" : forms.TextInput(attrs={'size': 50}),
            "Title" : forms.TextInput(attrs={'size': 50}),
            "Message": forms.Textarea(attrs={'size':100})
        }
