from django.shortcuts import render
from lab_2.models import Note
from django.http.response import HttpResponseRedirect

from lab_4.forms import NoteForm
# Create your views here.
def index(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)


def add_note(request):
    form = NoteForm()
    if request.method == 'POST':
        form = NoteForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-4')
    
    return render(request, 'lab4_form.html', {'form': form})

def note_list(request):
    notes = Note.objects.all().values()
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)