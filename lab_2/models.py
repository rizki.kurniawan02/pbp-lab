from django.db import models

# Create your models here.
class Note(models.Model):
    To = models.CharField(max_length=30)
    From = models.CharField(max_length=30)
    Title = models.CharField(max_length=50, default="", blank=True)
    Message = models.TextField() 