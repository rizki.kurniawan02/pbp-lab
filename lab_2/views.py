from django.shortcuts import render
from .models import Note
from django.http.response import HttpResponse
from django.core import serializers
import json as jsn

# Create your views here.
def index(request):
    notes = Note.objects.all().values()  # TODO Implement this
    response = {'notes': notes}
    return render(request, 'lab_2.html', response)

def xml(request):
    notes = Note
    data = serializers.serialize('xml', notes.objects.all())
    return HttpResponse(data, content_type="application/xml")

def json(request):
    notes = Note
    data = serializers.serialize('json', notes.objects.all())
    return HttpResponse(jsn.dumps(jsn.loads(data), indent=2), content_type="application/json")