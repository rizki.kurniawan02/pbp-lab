import './models/masker.dart';

List<Masker> dummyMasker = [
  Masker(
      id: 'm1',
      title: 'Masker Kuroyukihime',
      price: 99.99,
      rating: 5,
      stok: 10,
      imageUrl: 'test_image/Kuroyukihime.png',
      deksripsi: "Mantap"),
  Masker(
      id: 'm2',
      title: 'Masker Hiori Kazano',
      price: 99.99,
      stok: 10,
      rating: 5,
      imageUrl: 'test_image/Kazano.Hiori.full.2842993.jpg'),
  Masker(
      id: 'm3',
      title: 'Masker Maki',
      price: 99.99,
      rating: 5,
      stok: 10,
      imageUrl: 'test_image/Nishikino.Maki.full.1738560.jpg',
      deksripsi: 'Imi Wakanai'),
  Masker(
      id: 'm4',
      title: 'Masker Minalinsky',
      price: 100.00,
      rating: 5,
      stok: 1,
      imageUrl: 'test_image/Minami.Kotori.full.1655501.jpg',
      deksripsi: 'Best maid.'),
  Masker(
      id: 'm5',
      title: 'Masker Kuroyukihime',
      price: 99.99,
      rating: 5,
      stok: 10,
      imageUrl: 'test_image/Kuroyukihime.png',
      deksripsi: "Mantap"),
  Masker(
      id: 'm6',
      title: 'Masker Hiori Kazano',
      price: 99.99,
      stok: 10,
      rating: 5,
      imageUrl: 'test_image/Kazano.Hiori.full.2842993.jpg'),
  Masker(
      id: 'm7',
      title: 'Masker Maki',
      price: 99.99,
      rating: 5,
      stok: 0,
      imageUrl: 'test_image/Nishikino.Maki.full.1738560.jpg',
      deksripsi:
          'Imi Wakanai Imi Wakanai Imi Wakanai Imi Wakanai Imi Wakanai Imi Wakanai Imi Wakanai Imi Wakanai Imi Wakanai Imi Wakanai Imi Wakanai'),
  Masker(
      id: 'm8',
      title: 'Masker Minalinsky',
      price: 100000000000000000000000000000.00,
      rating: 5,
      stok: 1,
      imageUrl: 'test_image/Minami.Kotori.full.1655501.jpg',
      deksripsi: 'Best maid.'),
];
