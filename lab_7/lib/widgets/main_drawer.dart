import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MainDrawer extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  Widget buildListTile(String title, IconData icon, Function tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(children: <Widget>[
      Container(
        height: 120,
        width: double.infinity,
        padding: const EdgeInsets.all(20),
        alignment: Alignment.centerLeft,
        color: Theme.of(context).accentColor,
        child: const Text('Daftar Masker!',
            style: TextStyle(
              fontWeight: FontWeight.w900,
              fontSize: 30,
              color: Colors.white,
            )),
      ),
      const SizedBox(
        height: 20,
      ),
      buildListTile('Home', Icons.home, () {
        // Navigator.of(context).pushReplacementNamed('/');
      }),
      buildListTile('Wishlist', Icons.star, () {
        // Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
      }),
      buildListTile('Cart', Icons.shopping_cart, () {
        // Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
      }),
      buildListTile('Custom Mask', Icons.settings, () {
        // Navigator.of(context).pushReplacementNamed(FiltersScreen.routeName);
      }),
      Container(
        alignment: Alignment.center,
        padding: EdgeInsets.only(top: 50.0),
        child: Text("Subscribe To Our Newsletter",
            style: TextStyle(
              fontFamily: 'RobotoCondensed',
              fontSize: 16,
              fontWeight: FontWeight.bold,
            )),
      ),
      Form(
          key: _formKey,
          child: SingleChildScrollView(
              child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: "abcd@example.com",
                      labelText: "Email",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                    ),
                    validator: (value) {
                      if (value!.isEmpty || !value.contains("@")) {
                        return 'Please enter a valid email';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 20.0),
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Colors.green[900] as Color)),
                    onPressed: () {
                      // Validate returns true if the form is valid, or false otherwise.
                      if (_formKey.currentState!.validate()) {
                        // If the form is valid, display a snackbar. In the real world,
                        // you'd often call a server or save the information in a database.
                        ScaffoldMessenger.of(context).showSnackBar(
                          const SnackBar(
                            content: Text('Thank You'),
                          ),
                        );
                        print("Thank You");
                      }
                    },
                    child: const Text('Subscribe'),
                  ),
                ),
              ],
            ),
          ))),
      Container(
        alignment: Alignment.bottomCenter,
        padding: const EdgeInsets.all(20.0),
        child: Column(children: [
          const Text("Contact Us",
              style: TextStyle(
                fontFamily: 'RobotoCondensed',
                fontSize: 16,
                fontWeight: FontWeight.bold,
              )),
          Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                    icon: const FaIcon(FontAwesomeIcons.twitter),
                    onPressed: () => {},
                  ),
                  IconButton(
                    icon: const FaIcon(FontAwesomeIcons.facebook),
                    onPressed: () => {},
                  ),
                  IconButton(
                    icon: const FaIcon(FontAwesomeIcons.instagram),
                    onPressed: () => {},
                  )
                ],
              ))
        ]),
      )
    ]));
  }
}
