import 'package:flutter/material.dart';

class Masker {
  final String id;
  final String title;
  final double price;
  final int rating;
  int stok;
  final String deksripsi;
  final String imageUrl;

  Masker({
    required this.id,
    required this.title,
    required this.price,
    required this.rating,
    required this.imageUrl,
    required this.stok,
    this.deksripsi = "",
  });
}
