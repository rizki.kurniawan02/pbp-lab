import 'package:flutter/material.dart';

import '../dummy_data.dart';
import '../models/masker.dart';

class MaskerDetailScreen extends StatelessWidget {
  static const routeName = '/masker-detail';
  const MaskerDetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final maskerId = ModalRoute.of(context)!.settings.arguments as String;
    final selectedMasker =
        dummyMasker.firstWhere((masker) => masker.id == maskerId);
    return Scaffold(
        appBar: AppBar(
          title: Text(
            selectedMasker.title,
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        body: ListView(
          children: [
            Container(
                padding: const EdgeInsets.only(bottom: 25),
                alignment: Alignment.topCenter,
                child: Image.asset(
                  selectedMasker.imageUrl,
                  height: 250,
                  fit: BoxFit.fitWidth,
                )),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                children: [
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            for (var i = 0; i < selectedMasker.rating; i++)
                              const Icon(Icons.star,
                                  size: 32, color: Colors.deepOrange),
                          ],
                        ),
                        Text(
                          "\$${selectedMasker.price}",
                          style: const TextStyle(
                            fontSize: 28,
                            fontWeight: FontWeight.bold,
                          ),
                        )
                      ]),
                  Container(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      getStock(selectedMasker),
                      style: const TextStyle(fontSize: 24),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    padding:
                        const EdgeInsets.symmetric(vertical: 10, horizontal: 5),
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Deksripsi:\n${selectedMasker.deksripsi}",
                      textAlign: TextAlign.justify,
                      style: const TextStyle(
                        fontSize: 16,
                        color: Colors.grey,
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ));
  }

  String getStock(Masker selectedMasker) {
    if (selectedMasker.stok == 0) {
      return "Masker tidak tersedia";
    } else {
      return selectedMasker.stok.toString() + " masker tersedia";
    }
  }
}
