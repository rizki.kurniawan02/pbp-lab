// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:lab_7/widgets/main_drawer.dart';

import 'dart:core';

import 'masker_list_screen.dart';

class TabsScreen extends StatefulWidget {
  // final List<Meal> favoriteMeals;

  TabsScreen();

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  late List<Map<String, Object>> _pages;
  int _selectedPageIndex = 0;

  @override
  void initState() {
    _pages = [
      {
        "page": MaskerScreen(),
        'title': 'Home',
      },
      // {
      //   'page': FavoritesScreen(widget.favoriteMeals),
      //   'title': 'Kesukaan Saya',
      // },
    ];
    super.initState();
  }

  void _selectPage(int index) {
    setState(() {
      // _selectedPageIndex = index;
      _selectedPageIndex = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pages[_selectedPageIndex]['title'].toString()),
      ),
      drawer: MainDrawer(),
      body: _pages[_selectedPageIndex]['page'] as Widget,
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        backgroundColor: Theme.of(context).primaryColor,
        unselectedItemColor: Colors.white,
        selectedItemColor: Colors.white,
        currentIndex: _selectedPageIndex,
        selectedLabelStyle: TextStyle(color: Colors.white),
        // type: BottomNavigationBarType.fixed,
        items: [
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(
              Icons.home,
            ),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.star),
            label: 'Wishlist',
          ),
          BottomNavigationBarItem(
            backgroundColor: Theme.of(context).primaryColor,
            icon: Icon(Icons.shopping_cart),
            label: 'Cart',
          ),
        ],
      ),
    );
  }
}
