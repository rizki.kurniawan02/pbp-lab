import 'package:flutter/material.dart';

import 'dart:async';
import '../widgets/masker_item.dart';
import '../dummy_data.dart';

class MaskerScreen extends StatefulWidget {
  const MaskerScreen({Key? key}) : super(key: key);

  @override
  MaskerListScreen createState() => MaskerListScreen();
}

class MaskerListScreen extends State<MaskerScreen> {
  bool pertamax = true;
  int counter = 0;
  double _opacity1 = 0;
  final int _duration = 6;

  final headingFitur = ['Terpercaya', 'Higienis', 'Fashionable'];
  final contentFitur = [
    'Masker kami memiliki tingkat filtrasi hingga 70%',
    'Kesterilan produk masker dijaga hingga ke tangan pembeli',
    'Tidak hanya faktor kesehatan, kami selalu berusaha untuk menjaga faktor estetika produk masker kami',
  ];

  @override
  void initState() {
    if (pertamax) {
      setState(() {
        _opacity1 = 1;
        pertamax = false;
      });
    }
    fadeOut();
    fadeIn();
    super.initState();
  }

  void fadeOut() {
    Timer.periodic(Duration(milliseconds: _duration * 1000), (timer) {
      setState(() {
        _opacity1 = 0;
      });
    });
  }

  void fadeIn() {
    Timer.periodic(Duration(milliseconds: _duration * 1000), (timer) {
      Timer(const Duration(milliseconds: 495), () {
        setState(() {
          counter = (counter + 1) % 3;
          _opacity1 = 1;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return _myList();
  }

  CustomScrollView _myList() {
    return CustomScrollView(
      slivers: [
        SliverList(
            delegate: SliverChildBuilderDelegate(
          (BuildContext context, int index) {
            return SizedBox(
                child: Padding(
                    child: fiturMasker(), padding: const EdgeInsets.all(20)));
          },
          childCount: 1,
        )),
        SliverGrid(
            gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              maxCrossAxisExtent: 180,
              childAspectRatio: 0.50,
            ),
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                var mask = dummyMasker.elementAt(index);
                return Padding(
                  child: MaskerItem(
                    mask.id,
                    mask.title,
                    mask.price,
                    mask.rating,
                    mask.deksripsi,
                    mask.imageUrl,
                    mask.stok,
                  ),
                  padding: const EdgeInsets.all(15),
                );
              },
              childCount: dummyMasker.length,
            )),
        const SliverPadding(
          padding: EdgeInsets.all(10),
        ),
      ],
    );
  }

  GridView listMasker() {
    return GridView(
      physics: const NeverScrollableScrollPhysics(),
      padding: const EdgeInsets.all(30),
      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 180,
        childAspectRatio: 0.58,
        crossAxisSpacing: 20,
        mainAxisSpacing: 20,
      ),
      children: dummyMasker
          .map((mask) => MaskerItem(
                mask.id,
                mask.title,
                mask.price,
                mask.rating,
                mask.deksripsi,
                mask.imageUrl,
                mask.stok,
              ))
          .toList(),
    );
  }

  Container fiturMasker() {
    return Container(
        padding: const EdgeInsets.all(30),
        height: 200,
        width: 280,
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(25),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            AnimatedOpacity(
              child: Text(
                headingFitur[counter],
                textAlign: TextAlign.center,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              duration: const Duration(milliseconds: 500),
              opacity: _opacity1,
            ),
            AnimatedOpacity(
              child: Text(
                contentFitur[counter],
                textAlign: TextAlign.center,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              duration: const Duration(milliseconds: 500),
              opacity: _opacity1,
            ),
          ],
        ));
  }
}
