from django.shortcuts import get_object_or_404, render
from lab_2.models import Note
from django.core import serializers
from django.http.response import HttpResponse, HttpResponseNotFound
from .forms import NoteForm
from django.http import JsonResponse
import json
# Create your views here.

def index(request):
    notes = Note.objects.all().values()
    response = {'notes':notes}
    return render(request, 'lab5_index.html', response)

def get_note(request, id) :
    note = Note.objects.filter(id=id)
    data = serializers.serialize('json', note)

    if (data == "[]"):
        resp_str = "Note with id " + str(id) + " is not found"
        return HttpResponseNotFound(resp_str)

    return HttpResponse(json.dumps(json.loads(data), indent=2), content_type = "application/json")


def update_not(request, id) :
    obj = get_object_or_404(Note, id = id)
    form = NoteForm(request.POST or None, instance=obj)


