from django.urls import path
from .views import index, get_note

urlpatterns = [
    path('', index, name='index'),
    path('notes/<id>', get_note, name='get_note'),
]