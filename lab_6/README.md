# lab_6

Karena saat push lab_6 sebelumnya folder lib/ masih masuk daftar .gitignore, saya melakukan push ulang untuk menambahkan folde lib/.
Saya juga memasukan folder lib/ dalam zip file "lib_timestamp_original.zip" sebagai bukti bahwa lab_6 sudah selesai sebelum deadline (18 November 2021 20.00 WIB)

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
