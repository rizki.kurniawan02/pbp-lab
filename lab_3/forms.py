from django import forms
from django import forms
from django.forms import widgets
from lab_1.models import Friend
from django.utils.translation import gettext_lazy as _

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = "__all__"
        widgets = {'DOB' : widgets.SelectDateWidget(empty_label=("Tahun", "Bulan", "Hari"),)}
        labels = {'DOB': _("Date of Birth"),}        